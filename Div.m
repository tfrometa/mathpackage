function varargout = Div(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Div_OpeningFcn, ...
                   'gui_OutputFcn',  @Div_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function Div_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Div_OutputFcn(hObject, eventdata, handles)     
         varargout{1} = handles.output;       
         % Block generates the random numbers
         
         global N1 N2
                 
         n11=round(9*rand(1));  
         while (n11==0) || (n11==1)
           n11=round(9*rand(1));   
         end    
         n21=round(9*rand(1));    
         n22=round(9*rand(1)); n23=round(9*rand(1)); 
         N2=n21*100+n22*10+n23;                                            % Second hundreding number generated
         
         while (n21==0) || (rem(N2,n11)~=0) 
             n21=round(9*rand(1));
             n22=round(9*rand(1));
             n23=round(9*rand(1));             
             N2=n21*100+n22*10+n23;   
         end     
                
         set(handles.n11,'String',num2str(n11)); 
         set(handles.n21,'String',num2str(n21)); set(handles.n22,'String',num2str(n22));set(handles.n23,'String',num2str(n23));
         
         N1=n11;                                                           % First hundreding number generated
         
%%%%%%%%%%%% Cells of matrix of [6x3] %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function c11_Callback(hObject, eventdata, handles)
function c11_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c12_Callback(hObject, eventdata, handles)
function c12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c13_Callback(hObject, eventdata, handles)
function c13_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c21_Callback(hObject, eventdata, handles)
function c21_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c22_Callback(hObject, eventdata, handles)
function c22_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c23_Callback(hObject, eventdata, handles)
function c23_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c31_Callback(hObject, eventdata, handles)
function c31_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c32_Callback(hObject, eventdata, handles)
function c32_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c33_Callback(hObject, eventdata, handles)
function c33_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c41_Callback(hObject, eventdata, handles)
function c41_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c42_Callback(hObject, eventdata, handles)
function c42_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c43_Callback(hObject, eventdata, handles)
function c43_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c51_Callback(hObject, eventdata, handles)
function c51_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c52_Callback(hObject, eventdata, handles)
function c52_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c53_Callback(hObject, eventdata, handles)
function c53_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c61_Callback(hObject, eventdata, handles)
function c61_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c62_Callback(hObject, eventdata, handles)
function c62_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function c63_Callback(hObject, eventdata, handles)
function c63_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function d1_Callback(hObject, eventdata, handles)                          % Div  1 
    global d1 
    d1=str2num(get(hObject,'String'));       

function d1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function d2_Callback(hObject, eventdata, handles)                          % Div 2 
    global d2 
    d2=str2num(get(hObject,'String'));       

function d2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function d3_Callback(hObject, eventdata, handles)                          % Div 3 
    global d3 
    d3=str2num(get(hObject,'String'));       

function d3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Check_Callback(hObject, eventdata, handles)                       % For checking the result 
  
  global N1 N2 d1 d2 d3  
  
  result=N2/N1;
  div=d1*100+d2*10+d3;   
  textdiv=USftnumb(result);
  
  if div==result
      set(handles.Correct,'Visible','On');
  else
     set(handles.Incorrect,'Visible','On');
     set(handles.Result,'Visible','On','String',textdiv);
  end 
  set(handles.Check,'Visible','Off');
  set(handles.Othercal,'Visible','On');

function Othercal_Callback(hObject, eventdata, handles)                    % For repeating other calculation 
    set(handles.Correct,'Visible','Off');
    set(handles.Incorrect,'Visible','Off');
    set(handles.Result,'Visible','Off');
    set(handles.Check,'Visible','On');
    set(handles.Othercal,'Visible','Off'); 
    set(handles.d1,'String','0');  set(handles.d2,'String',' ');set(handles.d3,'String',' ');  
    set(handles.c11,'String',' '); set(handles.c12,'String',' '); set(handles.c13,'String',' ');
    set(handles.c21,'String',' '); set(handles.c22,'String',' '); set(handles.c23,'String',' '); 
    set(handles.c31,'String',' ');  set(handles.c32,'String',' ');set(handles.c33,'String',' ');  
    set(handles.c41,'String',' '); set(handles.c42,'String',' '); set(handles.c43,'String',' ');
    set(handles.c51,'String',' '); set(handles.c52,'String',' '); set(handles.c53,'String',' '); 
    set(handles.c61,'String',' ');  set(handles.c62,'String',' ');set(handles.c63,'String',' ');  
    Div 
    
  function tex=USftnumb(xnum)                                              % For writting in US format thounsan number, i. e separted by ","
    if xnum>=1000
        r=rem(xnum,1000);
        tp=(xnum-r)/1000;
        tex=[num2str(tp),',',num2str(r)];
    else
        tex=num2str(xnum); 
    end    
   
 
