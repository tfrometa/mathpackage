function varargout = Mult(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Mult_OpeningFcn, ...
                   'gui_OutputFcn',  @Mult_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function Mult_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Mult_OutputFcn(hObject, eventdata, handles)     
         varargout{1} = handles.output;       
         % Block generates the random numbers
         
         global N1 N2
         n12=round(9*rand(1));  n13=round(9*rand(1));   
         n22=round(9*rand(1));          
         n11=round(9*rand(1));  
         while n11==0
           n11=round(9*rand(1));   
         end    
         n21=round(9*rand(1));  
         while n21==0
           n21=round(9*rand(1));   
         end     
                
         set(handles.n11,'String',num2str(n11)); set(handles.n12,'String',num2str(n12));set(handles.n13,'String',num2str(n13));
         set(handles.n21,'String',num2str(n21)); set(handles.n22,'String',num2str(n22));
         
         N1=n11*100+n12*10+n13;                                            % First hundreding number generated
         N2=n21*10+n22;                                                    % Second hundreding number generated
        
         
function p11_Callback(hObject, eventdata, handles)                         % Prod 1,1         

function p11_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function add1_Callback(hObject, eventdata, handles)                        % For adding in the first time 
    set(handles.add2,'String',' ');
     
function add1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p12_Callback(hObject, eventdata, handles)                         % Prod 1,2
    
function p12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function add2_Callback(hObject, eventdata, handles)                        % For adding in the second time          
    
    set(handles.add1,'String',' ');
    
function add2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p13_Callback(hObject, eventdata, handles)                         % Prod 1,3
        
function p13_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p14_Callback(hObject, eventdata, handles)                         % Prod 1,4
       
function p14_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function p21_Callback(hObject, eventdata, handles)                         % Prod 2,1  
     
function p21_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p22_Callback(hObject, eventdata, handles)                         % Prod 2,2
    
function p22_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p23_Callback(hObject, eventdata, handles)                         % Prod 2,3
    
function p23_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p24_Callback(hObject, eventdata, handles)                         % Prod 2,4
        
function p24_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function s1_Callback(hObject, eventdata, handles)                          % Sum 1 
    global s1 
    s1=str2num(get(hObject,'String'));       

function s1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function s2_Callback(hObject, eventdata, handles)                          % Sum 2 
    global s2 
    s2=str2num(get(hObject,'String'));       

function s2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function s3_Callback(hObject, eventdata, handles)                          % Sum 3 
    global s3 
    s3=str2num(get(hObject,'String'));       

function s3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function s4_Callback(hObject, eventdata, handles)                          % Sum 4 
    global s4 
    s4=str2num(get(hObject,'String'));       

function s4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function s5_Callback(hObject, eventdata, handles)                          % Sum 5 
    global s5 
    s5=str2num(get(hObject,'String'));       

function s5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Check_Callback(hObject, eventdata, handles)                       % For checking the result 
  
  global N1 N2 s1 s2 s3 s4 s5 
  
  result=N1*N2;
  mult=s5*10000+s4*1000+s3*100+s2*10+s1;   
  textmult=USftnumb(result);
  
  if mult==result
      set(handles.Correct,'Visible','On');
  else
     set(handles.Incorrect,'Visible','On');
     set(handles.Result,'Visible','On','String',textmult);
  end 
  set(handles.Check,'Visible','Off');
  set(handles.Othercal,'Visible','On');

function Othercal_Callback(hObject, eventdata, handles)                    % For repeating other calculation 
    set(handles.Correct,'Visible','Off');
    set(handles.Incorrect,'Visible','Off');
    set(handles.Result,'Visible','Off');
    set(handles.Check,'Visible','On');
    set(handles.Othercal,'Visible','Off'); 
    set(handles.p11,'String',' '); set(handles.p12,'String',' ');
    set(handles.p13,'String',' '); set(handles.p14,'String',' ')
    set(handles.add1,'String',' '); set(handles.p21,'String',' '); 
    set(handles.p22,'String',' ');  set(handles.p23,'String',' '); 
    set(handles.p24,'String',' ');  set(handles.add2,'String',' ');
    set(handles.s1,'String',' ');  set(handles.s2,'String',' '); 
    set(handles.s3,'String',' ');  set(handles.s4,'String',' '); 
    set(handles.s5,'String','0');
    Mult 
    
  function tex=USftnumb(xnum)                                              % For writting in US format thounsan number, i. e separted by ","
    if xnum>=1000
        r=rem(xnum,1000);
        tp=(xnum-r)/1000;
        tex=[num2str(tp),',',num2str(r)];
    else
        tex=num2str(xnum); 
    end    
   
       
    
    
    
 
