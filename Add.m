function varargout = Add(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Add_OpeningFcn, ...
                   'gui_OutputFcn',  @Add_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function Add_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Add_OutputFcn(hObject, eventdata, handles)     
         varargout{1} = handles.output;       
         % Block generates the random numbers
         global add addd  
         addd=0; 
         n12=round(9*rand(1));  n13=round(9*rand(1)); 
         n22=round(9*rand(1));  n23=round(9*rand(1)); 
         n32=round(9*rand(1));  n33=round(9*rand(1));
         n11=round(9*rand(1));  
         while n11==0
           n11=round(9*rand(1));   
         end    
         n21=round(9*rand(1));  
         while n21==0
           n21=round(9*rand(1));   
         end    
         n31=round(9*rand(1));  
         while n31==0
           n31=round(9*rand(1));   
         end    
         set(handles.n11,'String',num2str(n11)); set(handles.n12,'String',num2str(n12));set(handles.n13,'String',num2str(n13));
         set(handles.n21,'String',num2str(n21)); set(handles.n22,'String',num2str(n22));set(handles.n23,'String',num2str(n23));
         set(handles.n31,'String',num2str(n31)); set(handles.n32,'String',num2str(n32));set(handles.n33,'String',num2str(n33));
         add=(n11+n21+n31)*100+(n12+n22+n32)*10+(n13+n23+n33);            % Sum of the three generated numbers
             
         
function su_Callback(hObject, eventdata, handles)                          % Add of the units 
    global su 
    su=str2num(get(hObject,'String'));     
      

function su_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function addd_Callback(hObject, eventdata, handles)                        % For adding to sum of the decinal numbers 
     global addd
     addd=str2num(get(hObject,'String'));
     if addd~=0
        set(handles.textadd,'String','0');    
    end    
       

function addd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function sd_Callback(hObject, eventdata, handles)                          % Add of decimal numbers 
    global sd 
    sd=str2num(get(hObject,'String'));
    

function sd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function addc_Callback(hObject, eventdata, handles)                        % For adding to sum of the centensimal numbers          
    global addd
    addc=str2num(get(hObject,'String'));    
    if (addc~=0) && (addd==0) 
        set(handles.addd,'String','0');
        set(handles.textadd,'String','0');
    end    
    
function addc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function sc_Callback(hObject, eventdata, handles)                          % Add of centensimal numbers 
    global sc 
    sc=str2num(get(hObject,'String')); 
   
    
function sc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function st_Callback(hObject, eventdata, handles)                          % Add of thaunbsand numbers 
    global st 
    st=str2num(get(hObject,'String')); 
   
    
function st_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Check_Callback(hObject, eventdata, handles)                       % For checking the result 
  
  global add st sc sd su  
  result=st*1000+sc*100+sd*10+su; 
  textAdd=USftnumb(add);
  if add==result
      set(handles.Correct,'Visible','On');
  else
     set(handles.Incorrect,'Visible','On');
     set(handles.Result,'Visible','On','String',textAdd);
  end 
  set(handles.Check,'Visible','Off');
  set(handles.Othercal,'Visible','On');

function Othercal_Callback(hObject, eventdata, handles)                    % For repeating other calculation 
    set(handles.Correct,'Visible','Off');
    set(handles.Incorrect,'Visible','Off');
    set(handles.Result,'Visible','Off');
    set(handles.Check,'Visible','On');
    set(handles.Othercal,'Visible','Off'); set(handles.st,'String','  ');
    set(handles.sc,'String','  '); set(handles.sd,'String',' ');
    set(handles.su,'String',' '); set(handles.addc,'String',' ');
    set(handles.addd,'String',' '); set(handles.textadd,'String',' ');   
    Add 
    
function tex=USftnumb(xnum)                                                % For writting in US format thounsan number, i. e separted by ","
    if xnum>=1000
        r=rem(xnum,1000);
        tp=(xnum-r)/1000;
        tex=[num2str(tp),',',num2str(r)];
    else
        tex=num2str(xnum); 
    end    
   
       
    