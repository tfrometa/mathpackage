function varargout = Mathpackage(varargin)

% This is main code that lets to acces to Table and Subt

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Mathpackage_OpeningFcn, ...
                   'gui_OutputFcn',  @Mathpackage_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Mathpackage is made visible.
function Mathpackage_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);


function varargout = Mathpackage_OutputFcn(hObject, eventdata, handles) 


varargout{1} = handles.output;
    
function SAdd_Callback(hObject, eventdata, handles)                        % Sum of simple numbers  
    SAdd

function Add_Callback(hObject, eventdata, handles)                         % Sum of three-digit numbers 
    Add

function Subt_Callback(hObject, eventdata, handles)                        % Subtraction of three-digit numbers
    Subt
 
function Table_Callback(hObject, eventdata, handles)                       % Multiplication table   
    Table
   
function Mult_Callback(hObject, eventdata, handles)                        % Multiplication of large numbers
    Mult

function Div_Callback(hObject, eventdata, handles)                         % Division of large numbers
    Div
