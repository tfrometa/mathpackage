function varargout = SAdd(varargin)


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SAdd_OpeningFcn, ...
                   'gui_OutputFcn',  @SAdd_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
% End initialization code - DO NOT EDIT

end

% --- Executes just before SAdd is made visible.
function SAdd_OpeningFcn(hObject, eventdata, handles, varargin);

handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = SAdd_OutputFcn(hObject, eventdata, handles) 
 global n1 n2  
 varargout{1} = handles.output;
   
         n1=round(99*rand(1));  n2=round(9*rand(1));
         while n1==0 || n1==1
             n1=round(99*rand(1));    
         end   
         while n2==0 || n2==1
             n2=round(9*rand(1));    
         end    
         set(handles.n1,'String',num2str(n1));
         set(handles.n2,'String',num2str(n2));
         r=rem(n1,10); ba=r+n2;
         set(handles.message1,'String',['Basic addition is (Suma basica es): ', num2str(r),' + ',num2str(n2),' = ',num2str(ba)]);
         set(handles.message2,'String',['The addition is the same to (La suma es lo mismo que): ',num2str(n2),' + ',num2str(n1)]); 
         
function sadd_Callback(hObject, eventdata, handles)
    global sadd 
    sadd=str2num(get(hObject,'String'));
    
function sadd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

 
function Check_Callback(hObject, eventdata, handles)
  
  global sadd n1 n2
  result=n1+n2;
  textsadd=num2str(result);
  
  if sadd==result    
      set(handles.Correct,'Visible','On');
      set(handles.msubt,'Visible','On');
      set(handles.msubt,'String',['Then (Entonces) ',num2str(sadd),' - ',num2str(n1),' = ',num2str(n2),' and (y) ',num2str(sadd),' - ',num2str(n2),' = ',num2str(n1)]);
  else
     set(handles.Incorrect,'Visible','On');
     set(handles.Result,'Visible','On','String',textsadd);
  end 
  set(handles.Check,'Visible','Off');
  set(handles.Othercal,'Visible','On');
  
  
function Othercal_Callback(hObject, eventdata, handles)      
    set(handles.Correct,'Visible','Off');
    set(handles.msubt,'Visible','Off');
    set(handles.Incorrect,'Visible','Off');
    set(handles.Result,'Visible','Off');
    set(handles.Check,'Visible','On');
    set(handles.Othercal,'Visible','Off'); 
    set(handles.sadd,'String',' ')
    SAdd
  
