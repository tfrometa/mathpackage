function varargout = Subt(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Subt_OpeningFcn, ...
                   'gui_OutputFcn',  @Subt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function Subt_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Subt_OutputFcn(hObject, eventdata, handles)     
         varargout{1} = handles.output;       
         % Block generates the random numbers
         global dif addd  
         addd=0; 
         n12=round(9*rand(1));   n13=round(9*rand(1));   
         n22=round(9*rand(1));   n23=round(9*rand(1));          
         n11=round(9*rand(1));  
         while n11==0
           n11=round(9*rand(1));   
         end    
         n21=round(9*rand(1));  
         while n21==0
           n21=round(9*rand(1));   
         end 
         dif=(n11-n21)*100+(n12-n22)*10+(n13-n23);                         % Dif of the two generated numbers
         
         if dif >=0         
             set(handles.n11,'String',num2str(n11)); set(handles.n12,'String',num2str(n12));set(handles.n13,'String',num2str(n13));
             set(handles.n21,'String',num2str(n21)); set(handles.n22,'String',num2str(n22));set(handles.n23,'String',num2str(n23));
         else
             set(handles.n11,'String',num2str(n21)); set(handles.n12,'String',num2str(n22));set(handles.n13,'String',num2str(n23));
             set(handles.n21,'String',num2str(n11)); set(handles.n22,'String',num2str(n12));set(handles.n23,'String',num2str(n13));
         end       
             
         
function du_Callback(hObject, eventdata, handles)                          % Subt of the units 
    global du 
    du=str2num(get(hObject,'String'));     
      

function du_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function addd_Callback(hObject, eventdata, handles)                        % For adding to sum of the decinal numbers 
     global addd
     addd=str2num(get(hObject,'String'));
     if addd~=0
        set(handles.textadd,'String','0');  
        set(handles.addd,'String','-1'); 
    end    
       

function addd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function dd_Callback(hObject, eventdata, handles)                          % Subt of decimal numbers 
    global dd 
    dd=str2num(get(hObject,'String'));
    

function dd_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function addc_Callback(hObject, eventdata, handles)                        % For adding to sum of the centensimal numbers          
    global addd
    addc=str2num(get(hObject,'String'));    
    if (addc~=0) && (addd==0) 
        set(handles.addd,'String','0');
        set(handles.textadd,'String','0');
        set(handles.addc,'String','-1');
    end    
    
function addc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dc_Callback(hObject, eventdata, handles)                          % Subt of centensimal numbers 
    global dc 
    dc=str2num(get(hObject,'String')); 
   
    
function dc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Check_Callback(hObject, eventdata, handles)                       % For checking the result 
  
  global dif dc dd du  
  subt=dc*100+dd*10+du;
  subt=abs(subt); 
  textdif=num2str(abs(dif));
  if abs(dif)==subt
      set(handles.Correct,'Visible','On');
  else
     set(handles.Incorrect,'Visible','On');
     set(handles.Result,'Visible','On','String',textdif);
  end 
  set(handles.Check,'Visible','Off');
  set(handles.Othercal,'Visible','On');

function Othercal_Callback(hObject, eventdata, handles)                    % For repeating other calculation 
    set(handles.Correct,'Visible','Off');
    set(handles.Incorrect,'Visible','Off');
    set(handles.Result,'Visible','Off');
    set(handles.Check,'Visible','On');
    set(handles.Othercal,'Visible','Off'); 
    set(handles.dc,'String','  '); set(handles.dd,'String',' ');
    set(handles.du,'String',' '); set(handles.addc,'String',' ');
    set(handles.addd,'String',' '); set(handles.textadd,'String',' ');   
    Subt 
       
    
    
    
   
    
    
