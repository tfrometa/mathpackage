function varargout = Table(varargin)


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Table_OpeningFcn, ...
                   'gui_OutputFcn',  @Table_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
% End initialization code - DO NOT EDIT

end

% --- Executes just before Table is made visible.
function Table_OpeningFcn(hObject, eventdata, handles, varargin);

handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = Table_OutputFcn(hObject, eventdata, handles) 
 global n1 n2  
 varargout{1} = handles.output;
   
         set(handles.mesg3,'Visible','Off');
         n1=round(9*rand(1));  n2=round(9*rand(1));
         while n1==0 || n1==1
             n1=round(9*rand(1));    
         end   
         while n2==0 || n2==1
             n2=round(9*rand(1));    
         end    
         set(handles.n1,'String',num2str(n1));
         set(handles.n2,'String',num2str(n2));
         set(handles.mesg1,'String',[num2str(n2),' x ',num2str(n1)]);      
         set(handles.mesg2,'String',mesg2(n1,n2));
  
         if n1==4
              set(handles.mesg3,'Visible','On');
              set(handles.mesg3,'String',['5 x ', num2str(n2),' - ',num2str(n2)]); 
         end 
         if n1==6
             set(handles.mesg3,'Visible','On');
             set(handles.mesg3,'String',['5 x ', num2str(n2),' + ',num2str(n2)]); 
         end 
         if n1==9
            set(handles.mesg3,'Visible','On');
            set(handles.mesg3,'String',['10 x ', num2str(n2),' - ',num2str(n2)]); 
         end          
     
  
function prod_Callback(hObject, eventdata, handles)
    global prod 
    prod=str2num(get(hObject,'String'));
    
function prod_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

 
function Check_Callback(hObject, eventdata, handles)
  
  global prod n1 n2  
  n1c=num2str(n1); n2c=num2str(n2); 
  prodn1n2=n1*n2;
  textprod=num2str(prodn1n2);
  
  if prod==prodn1n2    
      set(handles.Correct,'Visible','On');
      set(handles.mdiv,'Visible','On');
      set(handles.mdiv,'String',['Then (Entonces) ',num2str(prod),' : ',num2str(n1),' = ',num2str(n2),' and (y) ',num2str(prod),' : ',num2str(n2),' = ',num2str(n1)]);
  else
     set(handles.Incorrect,'Visible','On');
     set(handles.Result,'Visible','On','String',textprod);
  end 
  set(handles.Check,'Visible','Off');
  set(handles.Othercal,'Visible','On');
  
  
function Othercal_Callback(hObject, eventdata, handles)      
    set(handles.Correct,'Visible','Off');
    set(handles.mdiv,'Visible','Off');
    set(handles.Incorrect,'Visible','Off');
    set(handles.Result,'Visible','Off');
    set(handles.Check,'Visible','On');
    set(handles.Othercal,'Visible','Off'); 
    set(handles.prod,'String',' ')
    Table
  
function mesg2=mesg2(n1,n2)
   
    n2c=num2str(n2);
    mesg2='0';
    if n1==1
       mesg2=n2c;
    elseif n1==2
       mesg2=[n2c,' + ',n2c]; 
    elseif n1==3
       mesg2=[n2c,' + ',n2c,' + ',n2c];
    elseif n1==4
       mesg2=[n2c,' + ',n2c,' + ',n2c,' + ',n2c];
    elseif n1==5
       mesg2=[n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c]; 
    elseif n1==6
       mesg2=[n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c];
    elseif n1==7
       mesg2=[n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c];
    elseif n1==8
       mesg2=[n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c]; 
    elseif n1==9
       mesg2=[n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c,' + ',n2c]; 
    end    
        


    
